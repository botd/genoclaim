# BOTLIB - Framework to program bots.
#
# lo/tbl.py

modules = {
    "cfg": "bot.shw",
    "cmds": "bot.shw",
    "delete": "bot.rss",
    "display": "bot.rss",
    "feed": "bot.rss",
    "fetch": "bot.rss",
    "find": "bot.fnd",
    "fleet": "bot.shw",
    "log": "bot.ent",
    "meet": "bot.usr",
    "ps": "bot.shw",
    "rss": "bot.rss",
    "todo": "bot.ent",
    "up": "bot.shw",
    "users": "bot.usr",
    "v": "bot.shw"
}
names = {
    "cfg": [
        "bot.udp.Cfg",
        "bot.irc.Cfg",
        "bot.krn.Cfg"
    ],
    "dcc": [
        "bot.irc.DCC"
    ],
    "event": [
        "bot.irc.Event"
    ],
    "feed": [
        "bot.rss.Feed"
    ],
    "fetcher": [
        "bot.rss.Fetcher"
    ],
    "fleet": [
        "bot.flt.Fleet"
    ],
    "irc": [
        "bot.irc.IRC"
    ],
    "kernel": [
        "bot.krn.Kernel"
    ],
    "log": [
        "bot.ent.Log"
    ],
    "rss": [
        "bot.rss.Rss"
    ],
    "seen": [
        "bot.rss.Seen"
    ],
    "todo": [
        "bot.ent.Todo"
    ],
    "udp": [
        "bot.udp.UDP"
    ],
    "user": [
        "bot.usr.User"
    ],
    "users": [
        "bot.usr.Users"
    ]
}
